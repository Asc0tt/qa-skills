db.createUser(
    {
        user: "qa-skills-app",
        pwd: "qa-skills-app",
        roles: [
            {
                role: "readWrite",
                db: "qa-skills-db"
            }
        ]
    }
);