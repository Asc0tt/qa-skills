﻿namespace QaSkills.Contract.Error.Type
{
    public class HttpStatusCodeErrorType
    {
        public const string InternalServerError = @"https://qa-skills.com/api/common/errors/internal-server-error";
    }
}