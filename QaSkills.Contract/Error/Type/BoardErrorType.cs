﻿namespace QaSkills.Contract.Error.Type
{
    public class BoardErrorType
    {
        public const string GetBoardOverviewHandler = @"https://qa-skills.com/api/errors/get-board-overview";
        public const string GetBoardCardHandler = @"https://qa-skills.com/api/errors/get-board-card";
    }
}