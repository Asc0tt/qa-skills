﻿using System.Collections.Generic;

namespace QaSkills.Contract.Error
{
    public interface IHttpProblemDetail
    {
        string Type { get; }
        string Title { get; }
        int Status { get; }
        string Detail { get; }
        string Instance { get; set; }
        IDictionary<string, object> Extensions { get; }
    }
}