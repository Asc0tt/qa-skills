﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using QaSkills.Database.Core;
using QaSkills.Database.Settings;

namespace QaSkills.Database.Extensions
{
    public static class MongoDbClientConfigurationExtension
    {
        public static void UseQaSkillsDatabase(this IApplicationBuilder app)
        {
            var options = app.ApplicationServices.GetService<IOptions<MongoDbClusterNodes>>();
            var dbClient = app.ApplicationServices.GetService<IMongoDbClient>();

            dbClient.Initialize(options.Value.Nodes);
        }
    }
}