﻿using Microsoft.Extensions.DependencyInjection;
using QaSkills.Database.Core;
using QaSkills.Database.Models;
using QaSkills.Database.Repositories.BoardDatabase;

namespace QaSkills.Database.Extensions
{
    public static class MongoDbClientServiceExtension
    {
        public static void AddQaSkillsDatabase(this IServiceCollection services)
        {
            services.AddSingleton<IMongoDbClient, MongoDbClient>();
            
            //Repos
            services.AddTransient<IDbCollection<BoardItemModel>, BoardRepository>();
        }
    }
}