﻿using System.Collections.Generic;

namespace QaSkills.ServerErrorHandling
{
    public class IamATeapotException : ApiException
    {
        public IamATeapotException(string detail, string type): 
            this(detail, type, null, null)
        {
        }

        public IamATeapotException(string detail, string type, string instance): 
            this(detail, type, instance, null)
        {
        }

        public IamATeapotException(string detail, string type, string instance, IDictionary<string, object> extensions):
            base("Я чайник", 418, detail, type, instance, extensions)
        {
        }
    }
}