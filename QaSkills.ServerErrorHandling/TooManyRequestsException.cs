﻿using System.Collections.Generic;


namespace QaSkills.ServerErrorHandling
{
    public class TooManyRequestsException : ApiException
    {
        public TooManyRequestsException(string detail, string type): 
            this(detail, type, null, null)
        {
        }

        public TooManyRequestsException(string detail, string type, string instance): 
            this(detail, type, instance, null)
        {
        }

        public TooManyRequestsException(string detail, string type, string instance, IDictionary<string, object> extensions):
            base("Слишком много запросов", 429, detail, type, instance, extensions)
        {
        }
    }
}