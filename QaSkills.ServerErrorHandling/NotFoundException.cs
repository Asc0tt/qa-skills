﻿using System.Collections.Generic;

namespace QaSkills.ServerErrorHandling
{
    public class NotFoundException : ApiException
    {
        public NotFoundException(string detail, string type): 
            this(detail, type, null, null)
        {
        }

        public NotFoundException(string detail, string type, string instance): 
            this(detail, type, instance, null)
        {
        }

        public NotFoundException(string detail, string type, string instance, IDictionary<string, object> extensions):
            base("Ресурс не найден", 404, detail, type, instance, extensions)
        {
        }
    }

    public class ForbiddenException : ApiException
    {
        public ForbiddenException(string detail, string type): 
            this(detail, type, null, null)
        {
        }

        public ForbiddenException(string detail, string type, string instance): 
            this(detail, type, instance, null)
        {
        }

        public ForbiddenException(string detail, string type, string instance, IDictionary<string, object> extensions):
            base("Недостаточно прав доступа", 403, detail, type, instance, extensions)
        {
        }
    }
}