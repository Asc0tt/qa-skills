﻿using System.Collections.Generic;

namespace QaSkills.ServerErrorHandling
{
    public class InternalServerErrorException : ApiException
    {
        public InternalServerErrorException(string detail, string type): 
            this(detail, type, null, null)
        {
        }

        public InternalServerErrorException(string detail, string type, string instance): 
            this(detail, type, instance, null)
        {
        }

        public InternalServerErrorException(string detail, string type, string instance, IDictionary<string, object> extensions):
            base("Внутренняя ошибка сервера", 500, detail, type, instance, extensions)
        {
        }
    }
}