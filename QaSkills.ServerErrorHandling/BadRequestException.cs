﻿using System.Collections.Generic;

namespace QaSkills.ServerErrorHandling
{
    public class BadRequestException : ApiException
    {
        public BadRequestException(string detail, string type): 
            this(detail, type, null, null)
        {
        }

        public BadRequestException(string detail, string type, string instance): 
            this(detail, type, instance, null)
        {
        }

        public BadRequestException(string detail, string type, string instance, IDictionary<string, object> extensions):
            base(
                "Неверные параметры запроса", 
                400, 
                detail,
                type, 
                instance, 
                extensions)
        {
        }
    }
}