﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using QaSkills.Contract.Error.Type;

namespace QaSkills.ServerErrorHandling.Filter
{
    public class HttpResponseExceptionFilter : IActionFilter, IOrderedFilter
    {
        public int Order { get; set; } = int.MaxValue;

        public void OnActionExecuting(ActionExecutingContext context) { }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception is ApiException exception)
            {
                context.Result = new ObjectResult(exception.Details)
                {
                    StatusCode = exception.Status,
                };
                context.ExceptionHandled = true;
                return;
            }
            
            if (context.Exception != null)
            {
                var internalServerError = 
                    new InternalServerErrorException(
                        context.Exception.Message, 
                        HttpStatusCodeErrorType.InternalServerError);

                context.Result = new ObjectResult(internalServerError.Details)
                {
                    StatusCode = internalServerError.Status
                };
                context.ExceptionHandled = true;
                return;
            }
        }
    }
}