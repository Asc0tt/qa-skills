﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace QaSkills.ServerErrorHandling
{
    public abstract class ApiException : Exception
    {
        public int Status { get; }

        public ProblemDetails Details { get; }


        protected ApiException(string title, int statusCode, string detail, string type): 
            this(title, statusCode, detail, type, null, null)
        {
        }

        protected ApiException(string title, int statusCode, string detail, string type, string instance): 
            this(title, statusCode, detail, type, instance, null)
        {
        }

        protected ApiException(string title, int statusCode, string detail,  string type, string instance, IDictionary<string, object> extensions)
        {
            Status = statusCode;
            Details = new ProblemDetails
            {
                Status = statusCode,
                Title = title,
                Detail = detail,
                Type = type,
                Instance = instance
            };

            if (extensions == null || extensions.Count <= 0) 
                return;

            foreach (var extension in extensions)
            {
                Details.Extensions.Add(extension);
            }
        }
    }
}
