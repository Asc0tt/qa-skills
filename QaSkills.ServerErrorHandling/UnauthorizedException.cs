﻿using System.Collections.Generic;

namespace QaSkills.ServerErrorHandling
{
    public class UnauthorizedException : ApiException
    {
        public UnauthorizedException(string detail, string type): 
            this(detail, type, null, null)
        {
        }

        public UnauthorizedException(string detail, string type, string instance): 
            this(detail, type, instance, null)
        {
        }

        public UnauthorizedException(string detail, string type, string instance, IDictionary<string, object> extensions):
            base(
                "Пользователь не авторизован",
                401,
                detail,
                type,
                instance,
                extensions)
        {
        }
    }
}