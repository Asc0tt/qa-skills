﻿using System.Collections;
using System.Collections.Generic;

namespace QaSkills.SharedTools
{
    public static class TemplateExtensions
    {
        public static IList<T> WrapAsList<T>(this T obj)
        {
            return new List<T> { obj };
        }
    }
}