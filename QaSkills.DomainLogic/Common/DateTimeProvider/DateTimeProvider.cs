﻿using System;

namespace QaSkills.DomainLogic.Common.DateTimeProvider
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetUtcNow()
        {
            return DateTime.UtcNow;
        }
    }
}
