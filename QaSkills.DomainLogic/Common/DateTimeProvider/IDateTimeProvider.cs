﻿using System;

namespace QaSkills.DomainLogic.Common.DateTimeProvider
{
    public interface IDateTimeProvider
    {
        DateTime GetUtcNow();
    }
}