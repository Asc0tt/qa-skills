﻿using System;

namespace QaSkills.DomainLogic.Common.UserContext
{
    public class UserContext : IUserContext
    {
        public Guid GetRootId()
        {
            Guid.TryParse("c903cd20-9220-4e84-b1ad-611bf8331452", out var rootId);
            return rootId;
        }
    }
}
