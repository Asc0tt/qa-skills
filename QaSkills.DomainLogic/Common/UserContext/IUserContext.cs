﻿using System;

namespace QaSkills.DomainLogic.Common.UserContext
{
    public interface IUserContext
    {
        Guid GetRootId();
    }
}