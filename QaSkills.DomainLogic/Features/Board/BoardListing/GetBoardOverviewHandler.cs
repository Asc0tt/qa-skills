using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MediatR;
using QaSkills.Contract.Error.Type;
using QaSkills.Database.Models;
using QaSkills.Database.Repositories.BoardDatabase;
using QaSkills.DomainLogic.Common.UserContext;
using QaSkills.DomainLogic.Data;
using QaSkills.ServerErrorHandling;


namespace QaSkills.DomainLogic.Features.Board.BoardListing
{
    [UsedImplicitly]
    public class GetBoardOverviewHandler : IRequestHandler<BoardOverviewRequest, BoardOverview>
    {
        private readonly IDbCollection<BoardItemModel> _dbCollection;
        private readonly IUserContext _userContext;

        public GetBoardOverviewHandler(IDbCollection<BoardItemModel> dbCollection, IUserContext userContext)
        {
            _dbCollection = dbCollection;
            _userContext = userContext;
        }

        public async Task<BoardOverview> Handle(BoardOverviewRequest request, CancellationToken cancellationToken)
        {
            var rootId = _userContext.GetRootId();

            var exists = await _dbCollection.Get(m => m.RootId == rootId);
            if (exists.Count == 0)
            {
                await _dbCollection.Insert(new BoardItemModel
                {
                    Id = Guid.NewGuid(),
                    RootId = rootId,
                    ItemType = ItemType.Board
                });
            }

            var boarAndCards = await _dbCollection.Get(m => m.RootId == rootId);
            
            var board = boarAndCards.SingleOrDefault(bac => bac.ItemType == ItemType.Board);
            if (board == null)
                throw new NotFoundException($"������������� ����� �����: {rootId}", BoardErrorType.GetBoardOverviewHandler);

            //TODO: There is work for Automapper
            var boardOverview = new BoardOverview
            {
                BoardDetails = new BoardDetails {Id = board.Id},
                Cards = new List<BoardCardOverview>()
            };

            boardOverview.Cards.AddRange(
                boarAndCards
                    .Where(bac => bac.ItemType == ItemType.Card)
                    .Select(card => new BoardCardOverview
                    {
                        Id = card.Id,
                        Title = card.CardTitle
                    })
                );

            return boardOverview;
        }
    }
}