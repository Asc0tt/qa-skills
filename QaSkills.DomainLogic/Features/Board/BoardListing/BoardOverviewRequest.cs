using MediatR;
using QaSkills.DomainLogic.Data;

namespace QaSkills.DomainLogic.Features.Board.BoardListing
{
    public class BoardOverviewRequest : IRequest<BoardOverview>
    { }
}