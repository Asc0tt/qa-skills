using System;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MediatR;
using QaSkills.Database.Models;
using QaSkills.Database.Repositories.BoardDatabase;
using QaSkills.DomainLogic.Common.DateTimeProvider;
using QaSkills.DomainLogic.Data;
using QaSkills.DomainLogic.Notifications;

namespace QaSkills.DomainLogic.Features.BoardItem.DeleteBoardCard
{
    [UsedImplicitly]
    public class DeleteBoardItemHandler : AsyncRequestHandler<DeleteBoardItemCommand>
    {
        private readonly IMediator _mediator;
        private readonly IDbCollection<BoardItemModel> _dbCollection;
        private readonly IDateTimeProvider _dateTimeProvider;

        public DeleteBoardItemHandler(IMediator mediator, IDbCollection<BoardItemModel> dbCollection, IDateTimeProvider dateTimeProvider)
        {
            _mediator = mediator;
            _dbCollection = dbCollection;
            _dateTimeProvider = dateTimeProvider;
        }
        protected override async Task Handle(DeleteBoardItemCommand request, CancellationToken cancellationToken)
        {
            var deleteInfo = await _dbCollection.Delete(m => m.Id == request.Id);
            
            if(deleteInfo.Count == 0)
                throw new Exception($"Card with id: {request.Id} isn't exist");

            await _mediator.Publish(new EntityDeletedNotification<Guid>(
                nameof(BoardCard),
                request.Id,
                Guid.Empty,
                _dateTimeProvider.GetUtcNow()), cancellationToken);
        }
    }
}