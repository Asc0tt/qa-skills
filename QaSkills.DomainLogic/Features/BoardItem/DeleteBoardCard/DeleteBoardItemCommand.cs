using System;
using MediatR;

namespace QaSkills.DomainLogic.Features.BoardItem.DeleteBoardCard
{
    public class DeleteBoardItemCommand : IRequest
    {
        public Guid Id { get; }

        public DeleteBoardItemCommand(Guid id)
        {
            Id = id;
        }
    }
}