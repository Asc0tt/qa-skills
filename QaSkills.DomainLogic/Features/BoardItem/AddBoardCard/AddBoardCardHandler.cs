using System;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MediatR;
using QaSkills.Database.Models;
using QaSkills.Database.Repositories.BoardDatabase;
using QaSkills.DomainLogic.Common.DateTimeProvider;
using QaSkills.DomainLogic.Common.UserContext;
using QaSkills.DomainLogic.Data;
using QaSkills.DomainLogic.Notifications;

namespace QaSkills.DomainLogic.Features.BoardItem.AddBoardCard
{
    [UsedImplicitly]
    public class AddBoardCardHandler : IRequestHandler<AddBoardCardCommand, Guid>
    {
        private readonly IMediator _mediator;
        private readonly IDbCollection<BoardItemModel> _dbCollection;
        private readonly IUserContext _userContext;
        private readonly IDateTimeProvider _dateTimeProvider;

        public AddBoardCardHandler(IMediator mediator, 
            IDbCollection<BoardItemModel> dbCollection,
            IUserContext userContext,
            IDateTimeProvider dateTimeProvider)
        {
            _mediator = mediator;
            _dbCollection = dbCollection;
            _userContext = userContext;
            _dateTimeProvider = dateTimeProvider;
        }

        public async Task<Guid> Handle(AddBoardCardCommand command, CancellationToken cancellationToken)
        {
            var newId = Guid.NewGuid();

            await _dbCollection.Insert(
                new BoardItemModel
                {
                    Id = newId,
                    CardTitle = command.Data.Title,
                    ItemType = ItemType.Card,
                    RootId = _userContext.GetRootId()
                });
            
            await _mediator.Publish(new EntityAddedNotification<Guid>(
                    nameof(BoardCard),
                    newId,
                    Guid.Empty,
                    _dateTimeProvider.GetUtcNow()), cancellationToken);

            return newId;
        }
    }
}