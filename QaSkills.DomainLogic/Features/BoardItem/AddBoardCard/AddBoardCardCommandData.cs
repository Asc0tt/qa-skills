namespace QaSkills.DomainLogic.Features.BoardItem.AddBoardCard
{
    public class AddBoardCardCommandData
    {
        public string Title { get; set; }
    }
}