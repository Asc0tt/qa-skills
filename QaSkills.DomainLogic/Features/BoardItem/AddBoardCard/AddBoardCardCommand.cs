using System;
using MediatR;

namespace QaSkills.DomainLogic.Features.BoardItem.AddBoardCard
{
    public class AddBoardCardCommand : IRequest<Guid>
    {
        public AddBoardCardCommandData Data { get; }
        public AddBoardCardCommand(AddBoardCardCommandData data)
        {
            Data = data;
        }
    }
}