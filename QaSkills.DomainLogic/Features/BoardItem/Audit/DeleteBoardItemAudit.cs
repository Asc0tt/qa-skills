﻿using System;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MediatR;
using Microsoft.Extensions.Logging;
using QaSkills.DomainLogic.Notifications;

namespace QaSkills.DomainLogic.Features.BoardItem.Audit
{
    [UsedImplicitly]
    public class DeleteBoardItemAudit: INotificationHandler<EntityDeletedNotification<Guid>>
    {
        private readonly ILogger<DeleteBoardItemAudit> _logger;

        public DeleteBoardItemAudit(ILogger<DeleteBoardItemAudit> logger)
        {
            _logger = logger;
        }
        public Task Handle(EntityDeletedNotification<Guid> notification, CancellationToken cancellationToken)
        {
            _logger.LogDebug($"[AUDIT IMITATION]: Deleted {notification.EntityName} with id:'{notification.EntityId}' at {notification.Moment} by user with id:'{notification.UserId}'");
            return Task.CompletedTask;
        }
    }
}