using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using JetBrains.Annotations;
using MediatR;
using QaSkills.Contract.Error.Type;
using QaSkills.Database.Models;
using QaSkills.Database.Repositories.BoardDatabase;
using QaSkills.DomainLogic.Data;
using QaSkills.ServerErrorHandling;

namespace QaSkills.DomainLogic.Features.BoardItem.GetBoardCard
{
    [UsedImplicitly]
    public class GetBoardCardHandler : IRequestHandler<GetBoardCardRequest, BoardCard>
    {
        private readonly IDbCollection<BoardItemModel> _dbCollection;

        public GetBoardCardHandler(IDbCollection<BoardItemModel> dbCollection)
        {
            _dbCollection = dbCollection;
        }
        public async Task<BoardCard> Handle(GetBoardCardRequest request, CancellationToken cancellationToken)
        {
            var card = await _dbCollection.Get(m => m.Id == request.Id);
            if(card.Count == 0)
                    throw new NotFoundException($"������������� ��������: {request.Id}", BoardErrorType.GetBoardCardHandler);

            var result = card.Single();

            return new BoardCard
            {
                Id = result.Id,
                Title = result.CardTitle
            };
        }
    }
}