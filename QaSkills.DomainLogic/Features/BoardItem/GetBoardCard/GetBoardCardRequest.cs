using System;
using MediatR;
using QaSkills.DomainLogic.Data;

namespace QaSkills.DomainLogic.Features.BoardItem.GetBoardCard
{
    public class GetBoardCardRequest : IRequest<BoardCard>
    {
        public Guid Id { get; }

        public GetBoardCardRequest(Guid id)
        {
            Id = id;
        }
    }
}