﻿using System.Collections.Generic;

namespace QaSkills.DomainLogic.Data
{
    public class BoardOverview
    {
        public BoardDetails BoardDetails { get; set; }
        public List<BoardCardOverview> Cards { get; set; }
    }
}