﻿using System;

namespace QaSkills.DomainLogic.Data
{
    public class BoardCard
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}