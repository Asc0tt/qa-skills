using System;

namespace QaSkills.DomainLogic.Data
{
    public class BoardCardOverview
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
    }
}