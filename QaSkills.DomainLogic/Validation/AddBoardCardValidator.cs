﻿using FluentValidation;
using QaSkills.DomainLogic.Features.BoardItem.AddBoardCard;

namespace QaSkills.DomainLogic.Validation
{
    public class AddBoardCardValidator : AbstractValidator<AddBoardCardCommandData>
    {
        public AddBoardCardValidator()
        {
            RuleFor(x => x.Title)
                .NotNull()
                .NotEmpty()
                .WithMessage(_ => "Заголовок карточки не может быть пустым");

            RuleFor(x => x.Title)
                .MaximumLength(50)
                .WithMessage(_ => "Заголовок карточки не может быть длиннее 50 символов");
        }
    }
}