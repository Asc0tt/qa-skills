﻿using System;
using MediatR;

namespace QaSkills.DomainLogic.Notifications
{
    public class EntityAddedNotification<TId> : INotification
    {
        public string EntityName { get; }
        public TId EntityId { get; }
        public Guid UserId { get; }
        public DateTime Moment { get; }

        public EntityAddedNotification(string entityName, TId entityId, Guid userId, DateTime moment)
        {
            EntityName = entityName;
            EntityId = entityId;
            UserId = userId;
            Moment = moment;
        }
    }
}