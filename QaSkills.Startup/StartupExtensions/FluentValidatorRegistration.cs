﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using QaSkills.DomainLogic.Features.BoardItem.AddBoardCard;
using QaSkills.DomainLogic.Validation;

namespace QaSkills.Startup.StartupExtensions
{
    public static class FluentValidatorRegistration
    {
        public static void AddValidators(this IServiceCollection services)
        {
            services.AddTransient<IValidator<AddBoardCardCommandData>, AddBoardCardValidator>();
        }
    }
}