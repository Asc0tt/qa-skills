﻿using Microsoft.Extensions.DependencyInjection;
using QaSkills.DomainLogic.Common.DateTimeProvider;
using QaSkills.DomainLogic.Common.UserContext;

namespace QaSkills.Startup.StartupExtensions
{
    public static class ServiceRegistration
    {
        public static void AddApplicationDependencies(this IServiceCollection services)
        {
            services.AddTransient<IDateTimeProvider, DateTimeProvider>();
            services.AddTransient<IUserContext, UserContext>();
        }
    }
}