﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace QaSkills.Startup.Controllers
{
    [ApiController]
    [AllowAnonymous]
    public class HomeController : ControllerBase
    {
        private const string IndexHtml = "<html><body bgcolor='#000000' alink='#FFFFFF' link='#FFFFFF' vlink='#FFFFFF'>" +
                                         "<font color='#FFFFFF' face='Segoe UI'" +
                                         "<p><a href=\"./swagger\">Swagger</a></p>" +
                                         "<p><a href=\"http://localhost:8081\">Mongo</a></p>" +
                                         "</font></body></html>";
        
        [Route("")]
        [Route("Home")]
        [Route("Home/Index")]
        [HttpGet]
        public IActionResult Index()
        {
            return new ContentResult 
            {
                ContentType = "text/html",
                Content = IndexHtml
            };
        }
    }
}