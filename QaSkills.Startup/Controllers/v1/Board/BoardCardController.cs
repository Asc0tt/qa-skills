﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using QaSkills.DomainLogic.Data;
using QaSkills.DomainLogic.Features.BoardItem.AddBoardCard;
using QaSkills.DomainLogic.Features.BoardItem.DeleteBoardCard;
using QaSkills.DomainLogic.Features.BoardItem.GetBoardCard;

namespace QaSkills.Startup.Controllers.v1.Board
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BoardCardController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        public BoardCardController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<BoardCard> Get(Guid id)
        {
            return await _mediator.Send(new GetBoardCardRequest(id));
        }

        [HttpPost(nameof(Add))]
        public async Task<Guid> Add(AddBoardCardCommandData data)
        {
            var newId = await _mediator.Send(new AddBoardCardCommand(data));
            return newId;
        }


        [HttpPost(nameof(Delete))]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _mediator.Send(new DeleteBoardItemCommand(id));
            return Ok();
        }
    }
}