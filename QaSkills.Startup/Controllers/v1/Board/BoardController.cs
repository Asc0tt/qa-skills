﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using QaSkills.DomainLogic.Data;
using QaSkills.DomainLogic.Features.Board.BoardListing;

namespace QaSkills.Startup.Controllers.v1.Board
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class BoardController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        public BoardController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet(nameof(Overview))]
        public async Task<BoardOverview> Overview()
        {
            return await _mediator.Send(new BoardOverviewRequest());
        }
    }
}

