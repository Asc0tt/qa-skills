using System.Reflection;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using QaSkills.Database.Extensions;
using QaSkills.Database.Settings;
using QaSkills.ServerErrorHandling.Filter;
using QaSkills.Startup.StartupExtensions;

namespace QaSkills.Startup
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Framework
            services.AddLogging();
            services.AddControllers(options =>
                options.Filters.Add(new HttpResponseExceptionFilter()));
            services
                .AddMvc()
                .AddFluentValidation();
            
            //Third party Packages
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });
            services.AddMediatR(Assembly.Load("QaSkills.DomainLogic"));
            
            //Options
            services
                .Configure<MongoDbClusterNodes>(options => 
                    Configuration.GetSection(nameof(MongoDbClusterNodes)).Bind(options));

            //InSolutionReferences
            services.AddQaSkillsDatabase();
            
            //DI
            services.AddApplicationDependencies();
            services.AddValidators();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseQaSkillsDatabase();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
