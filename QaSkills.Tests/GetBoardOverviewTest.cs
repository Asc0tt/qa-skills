using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using QaSkills.Database.Models;
using QaSkills.Database.Repositories.BoardDatabase;
using QaSkills.DomainLogic.Common.UserContext;
using QaSkills.DomainLogic.Features.Board.BoardListing;
using QaSkills.SharedTools;

namespace QaSkills.Tests
{
    public class GetBoardOverviewTest
    {
        private readonly GetBoardOverviewHandler _getGetBoardOverviewHandler;

        public GetBoardOverviewTest()
        {
            var rootId = Guid.NewGuid();
            var boardModel = new BoardItemModel
            {
                Id = Guid.Empty,
                ItemType = ItemType.Board,
                CardTitle = "",
                RootId = rootId
            };

            var boardItemsRepositoryMock = new Mock<IDbCollection<BoardItemModel>>(MockBehavior.Strict);
            boardItemsRepositoryMock
                .Setup(collection => collection.Get(It.IsAny<Expression<Func<BoardItemModel, bool>>>()))
                .ReturnsAsync(boardModel.WrapAsList());
            
            var userContextMock = new Mock<IUserContext>(MockBehavior.Strict);
            userContextMock
                .Setup(context => context.GetRootId())
                .Returns(rootId);

            _getGetBoardOverviewHandler = new GetBoardOverviewHandler(
                boardItemsRepositoryMock.Object, 
                userContextMock.Object);
        }

        [Test]
        public async Task When_GetBoardOverview_Should_ReturnsBoardWithDetailsAndCardsIsEmptyButNotNull()
        {
            var boardOverview = await _getGetBoardOverviewHandler.Handle(new BoardOverviewRequest(), CancellationToken.None);
            Assert.That(boardOverview, Is.Not.Null);
            Assert.That(boardOverview.BoardDetails, Is.Not.Null);
            Assert.That(boardOverview.Cards, Is.Empty);
        }
    }
}