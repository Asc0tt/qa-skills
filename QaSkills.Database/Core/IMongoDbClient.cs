﻿using System.Collections.Generic;
using MongoDB.Driver;
using QaSkills.Database.Settings;

namespace QaSkills.Database.Core
{
    public interface IMongoDbClient
    {
        IMongoDatabase GetDataBase(string dbName);
        void Initialize(ICollection<MongoDbServer> dbConnectionSettingsList);
    }
}