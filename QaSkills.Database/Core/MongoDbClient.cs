﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using MongoDB.Driver.Core.Configuration;
using QaSkills.Database.Settings;
using QaSkills.SharedTools;

namespace QaSkills.Database.Core
{
    public class MongoDbClient : IMongoDbClient
    {
        private MongoClient _client;
        private bool _isInitialized;

        public IMongoDatabase GetDataBase(string dbName)
        {
            if (!_isInitialized)
            {
                throw new Exception("MongoDB client isn't initialized");
            }

            return _client.GetDatabase(dbName);
        }
       
        public void Initialize(ICollection<MongoDbServer> dbConnectionSettingsList)
        {
            if (dbConnectionSettingsList == null) 
                throw new ArgumentNullException(nameof(dbConnectionSettingsList));
            
            var mongoServerAddresses = new List<MongoServerAddress>(dbConnectionSettingsList.Count);

            if (mongoServerAddresses.Any(serverAddress 
                => serverAddress.Host.IsNullOrWhiteSpace()))
            {
                throw new ArgumentNullException(
                    nameof(MongoDbServer.Host),
                    "Invalid MongoDB host name");
            }

            if (mongoServerAddresses.Any(serverAddress 
                => serverAddress.Port < 0 || serverAddress.Port > 0xFFFF))
            {
                throw new ArgumentOutOfRangeException(
                    nameof(MongoDbServer.Port),
                    "Invalid MongoDB port number");
            }

            mongoServerAddresses.AddRange(
                dbConnectionSettingsList.Select(
                    connectionSetting => 
                        new MongoServerAddress(
                            connectionSetting.Host,
                            connectionSetting.Port)));

            InitializeInternal(mongoServerAddresses);
        }

        private void InitializeInternal(IEnumerable<MongoServerAddress> mongoServerAddresses)
        {
            var serverAddress = mongoServerAddresses.First();
            const string username = "qa-skills-app";
            const string password = "qa-skills-app";
            const string databaseName = "qa-skills-db";
            
            var settings = new MongoClientSettings
            {
                Credential = MongoCredential.CreateCredential(databaseName, username, password),
                Server = new MongoServerAddress(serverAddress.Host, serverAddress.Port)
            };

            _client = new MongoClient(settings);   
            _isInitialized = true;
        }
    }
}