﻿namespace QaSkills.Database.Settings
{
    public class MongoDbServer
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }
}