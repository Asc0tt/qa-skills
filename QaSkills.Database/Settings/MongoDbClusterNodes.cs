﻿using System.Collections.Generic;

namespace QaSkills.Database.Settings
{
    public class MongoDbClusterNodes
    {
        public List<MongoDbServer> Nodes { get; set; }
    }
}