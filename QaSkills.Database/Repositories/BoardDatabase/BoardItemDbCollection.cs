﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using QaSkills.Database.Core;
using QaSkills.Database.Models;

namespace QaSkills.Database.Repositories.BoardDatabase
{
    public class BoardRepository : IDbCollection<BoardItemModel>
    {
        private const string DbName = "qa-skills-db";

        private readonly SharpCompress.Lazy<IMongoCollection<BoardItemModel>> _collection;
        public BoardRepository(IMongoDbClient mongoDbClient)
        {
            _collection = new SharpCompress.Lazy<IMongoCollection<BoardItemModel>>(() => GetLazyCollection(mongoDbClient));
        }

        private static IMongoCollection<BoardItemModel> GetLazyCollection(IMongoDbClient mongoDbClient)
        {
            return mongoDbClient
                .GetDataBase(DbName)
                .GetCollection<BoardItemModel>(nameof(BoardItemModel).Replace("Model", ""));
        }


        public async Task<IList<BoardItemModel>> Get(Expression<Func<BoardItemModel, bool>> predicate)
        {
            var filter = Builders<BoardItemModel>.Filter.Where(predicate);
            return await _collection.Value.Find(filter).ToListAsync();
        }

        public async Task Insert(BoardItemModel model)
        {
            await _collection.Value.InsertOneAsync(model);
        }

        public async Task<DeleteInfo> Delete(Expression<Func<BoardItemModel, bool>> predicate)
        {
            var filter = Builders<BoardItemModel>.Filter.Where(predicate);
            var deleteResult = await _collection.Value.DeleteOneAsync(filter);
            return new DeleteInfo
            {
                Count = deleteResult.DeletedCount
            };
        }
    }

    public interface IDbCollection<TModel>
    {
        Task<IList<TModel>> Get(Expression<Func<TModel, bool>> predicate);
        Task Insert(TModel model);
        Task<DeleteInfo> Delete(Expression<Func<BoardItemModel, bool>> predicate);
    }

    public class DeleteInfo
    {
        public long Count { get; set; }
    }
}