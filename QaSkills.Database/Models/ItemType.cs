﻿namespace QaSkills.Database.Models
{
    public enum ItemType
    {
        Unknown,
        Board,
        Card
    }
}