﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace QaSkills.Database.Models
{
    public class BoardItemModel
    {
        public Guid Id { get; set; }
        public Guid RootId { get; set; }
        public ItemType ItemType { get; set; }
        public string CardTitle { get; set; }
    }
}
